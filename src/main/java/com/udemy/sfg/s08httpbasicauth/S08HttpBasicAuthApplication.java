package com.udemy.sfg.s08httpbasicauth;

import com.udemy.sfg.s08httpbasicauth.security.dtos.CustomAuthority;
import com.udemy.sfg.s08httpbasicauth.security.dtos.CustomUser;
import com.udemy.sfg.s08httpbasicauth.security.repositories.CustomAuthorityRepository;
import com.udemy.sfg.s08httpbasicauth.security.repositories.CustomUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import java.util.Arrays;

@SpringBootApplication
public class S08HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S08HttpBasicAuthApplication.class, args);
    }
}
