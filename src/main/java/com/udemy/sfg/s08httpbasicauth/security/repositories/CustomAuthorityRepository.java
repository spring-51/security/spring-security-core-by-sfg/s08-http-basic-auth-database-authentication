package com.udemy.sfg.s08httpbasicauth.security.repositories;

import com.udemy.sfg.s08httpbasicauth.security.dtos.CustomAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomAuthorityRepository extends JpaRepository<CustomAuthority, Integer> {
}
