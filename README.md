# Http Basic Auth Database(using JPA) Authentication

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

## L49 - DB Authentication Overview
```
refer ./L49-SpringSecurity.pdf
```

## L50 - JPA Entities
```
1. Imp interfaces for a user in spring security
  - UserDetails
    -- spring has a class User hich implemets this
      --- User - This is a immutable class
      --- Spring Security uses this internally for keeping username, password , roles etc.
  
  - GrantedAuthority
    -- aiuthorities stoles role info in Spring security
    -- will be discussed later
  - UserDetailService 
    -- will be studied later (during L54)

2. We need to create entities whic pull data from db and map it Spring security internal model class.
  - CustomUser ~ Spring "User"
    -- refer - com.udemy.sfg.s08httpbasicauth.security.dtos.CustomUser
  - CustomAuthority ~ Spring "SimpleGrantedAuthority"
      -- refer - com.udemy.sfg.s08httpbasicauth.security.dtos.CustomAuthority


TO BE CONTINUED .....
```

## L51 - Project lombok configiration
```
1. use of @Singular
  - we use this with @Builder
eg.
  class Person{
      @Singular private final List<String> interests;
  }
  -----------------------------------------------
  Person person = Person.builder()
  .interest("history")
  .interest("sport")
  .build();
  ------------------------------------------------
    -refer - https://www.baeldung.com/lombok-builder-singular


TO BE CONTINUED .....
```

## L52 - Spring Data JPA REpositories
```
1. We have added two data jpa repsitories
  - CustomUserRepository for CustomUser 
    -- refer - com.udemy.sfg.s08httpbasicauth.security.repositories.CustomUserRepository
  - CustomAuthorityRepository for CustomAuthority
    -- refer - com.udemy.sfg.s08httpbasicauth.security.repositories.CustomAuthorityRepository


TO BE CONTINUED .....
```

## Assignment - Load user in h2 db
```
1. add h2 dependency in pom.xml
2. Create a class UserDataLoader and implements CommandLineRunner
3. add @Component annotation on UserDataLoader
4. override run (...) of CommandLineRunner interface
  - refer - com.udemy.sfg.s08httpbasicauth.bootstrap.UserDataLoader

```

## L53 - H2 db console access
```
1. When we add spring security , h2 console by default restricted by spring security.
2. We need to make h2 console urls as whitelist 
  - Step1 -  add spring.h2.console.enabled=true in application properties file
  - Step2 - whitelist /h2-cosole/** as antmatcher
  http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for h2 -console
                    .antMatchers("/h2-console/**").permitAll()
            ;
        });
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
  
  - Step3 - add http.headers().frameOptions().sameOrigin(); in  SecurityConfig -> configure(HttpSecurity)
    -- this is required since h2 console uses iframe, iFrame are restricted by spring sec.


TO BE CONTINUED .....
```

## L54 - User details service
```
1. USerDetailService
  - this is spring provided spring secutity interface
  - this is used to get user dto using username, it has only one abstract method
    -- UserDetails loadUserByUsername(String s)

2. We need to create our own User detail service which shold load user from our db
  - refer - com.udemy.sfg.s08httpbasicauth.security.services.CustomJpaUserDetailsService 
  
  
TO BE CONTINUED .....
```

## L55 - Spring Security Configuration
```
1. Till now we are using in memory authentication, now we have placed our user table in place.
Now we can migrate in memory authentication to database authentication.

2. To do that we need to modify com.udemy.sfg.s08httpbasicauth.configs.SecurityConfig -> configure(AuthenticationManagerBuilder)
  - we need to remove inMemoryAuthentication
  
3. When we have only one user details service, we have 2 ways to configure user details service 
  - way1 - remove SecurityConfig -> configure(AuthenticationManagerBuilder) from SecurityConfig
    -- Spring auto configure UserDetailsService to be used as authentication and wire it up with spring security
  - way2
    -- step1 - autowire  UserDetailsService in SecurityConfig 
    -- step2 - keep SecurityConfig -> configure(AuthenticationManagerBuilder) from SecurityConfig
               and add auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurityConfig -> userDetailsService
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurityConfig -> configure(AuthenticationManagerBuilder)
               
  - FOR THE SAKE OF GOOD PRACTICE WE WRE GOING WITH WAY2.

[WE HAVE MIGRATED SPRING SEC FROM IN MEM TO DB AUTHENTICATION]
```

## L56 - JUNIT test failure fix
```
1. Use @SpringBootTest in place of @WebMvcTest
  - @WebMvcTest load minimum config for  mvc tests, it does not load Spring Data JPA, Custom user details service etc
    that's why when we migrated spring sec from in-mem to db authentication @WebMvcTest will not work, 
    that's why we use @SpringBootTest in place of @WebMvcTest
```

## EXTRA BY SELF
```
1. I have added two more user deatil service
  - first one is using CustomerUser2 entity for credential
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurtyConfig -> configure(AuthenticationManagerBuilder)
    -- with above config I have added Entities , Repo, second UserDetailsService
    -- JUNIT tests
       --- refer StudentControllerTests -> testHttpBasicInMemAuthFromInMemUserDetailsEntityUsingUser1()
       --- refer StudentControllerTests -> testHttpBasicInMemAuthFromInMemUserDetailsEntityUsingUser2()
       
  - second one is using in memory user details service for credential
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurtyConfig -> configure(AuthenticationManagerBuilder)
    -- refer - com.udemy.sfg.s08httpbasicauth.configs.SecurtyConfig -> userDetailsService()
    -- JUNIT tests
       --- refer StudentControllerTests -> testHttpBasicDbAuthFromCustomUser2Entity()
       
    
```